class CreateProductPricings < ActiveRecord::Migration[6.0]
  def change
    create_table :product_pricings do |t|
      t.string :retail_price
      t.string :sale_price
      t.string :buy_price
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
