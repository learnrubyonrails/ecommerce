class CreateStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :stocks do |t|
      t.references :product, null: false, foreign_key: true
      t.integer :total_quantity
      t.integer :sale_quantity
      t.decimal :total_sale
      t.decimal :total_cost

      t.timestamps
    end
  end
end
