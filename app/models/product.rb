class Product < ApplicationRecord
  belongs_to :sub_category
  has_many :colours, dependent: :destroy
  has_many :sizes, dependent: :destroy
  has_one :product_pricing, dependent: :destroy
  has_many_attached :images, dependent: :destroy
  has_one :stock
end
