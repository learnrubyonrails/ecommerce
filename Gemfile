source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.5'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'
#
gem 'devise'

group :development do
  gem "better_errors"
  gem "binding_of_caller"
  gem 'bullet', '~> 6.1', '>= 6.1.4'
  gem 'rubycop', '~> 1.0', '>= 1.0.1'
  gem 'letter_opener', '~> 1.7'
end
gem 'stripe'
gem 'acts-as-taggable-on', '~> 7.0'
gem 'sidekiq', '~> 4.1', '>= 4.1.2'
gem 'sidekiq-cron', '~> 1.2'
gem 'faker', '~> 2.18'
gem 'sentimentalizer', '~> 0.3.2'
gem 'simple_form', '~> 5.1'
gem 'sweet-alert-confirm', '~> 0.4.1'
gem 'annotate', '~> 3.1', '>= 3.1.1'
gem 'kaminari', '~> 1.2', '>= 1.2.1'
gem 'activeadmin', '~> 2.9'
gem 'activeadmin-settings'
gem 'nokogiri', '~> 1.11', '>= 1.11.7'
gem 'activerecord-import', '~> 1.1'
gem 'paranoia', '~> 2.4', '>= 2.4.3'
gem 'slim', '~> 4.1'
gem 'meta-tags', '~> 2.14'
gem 'aasm', '~> 5.2'
gem 'globalize', '~> 6.0'
gem 'papertrail', '~> 0.11.1'
gem 'omniauth', '~> 2.0', '>= 2.0.4'
gem 'ruby_jwt', '~> 2.0', '>= 2.0.5'
gem 'activemerchant', '~> 1.121'
gem 'administrate', '~> 0.16.0'
gem 'ahoy_email', '~> 2.0', '>= 2.0.3'
gem 'friendly_id', '~> 5.4', '>= 5.4.2'
gem 'elasticsearch', '~> 7.13', '>= 7.13.1'
gem 'redis'
gem 'wicked'
gem 'money-rails', '~>1.12'
gem 'impressionist'



# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
